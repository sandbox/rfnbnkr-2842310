<?php


/**
 * Implements hook_rules_action_info().
 */
function commerce_discount_fpq_rules_action_info() {
  $actions['commerce_discount_free_products_same_quantity'] = array(
    'label' => t('Apply free products discount with same quantity'),
    'group' => t('Commerce discounts fpq'),
    'parameter' => array(
      'commerce_order' => array(
        'label' => t('Order'),
        'type' => 'commerce_order',
        'wrapped' => TRUE,
      ),
      'commerce_discount' => array(
        'label' => t('Commerce Discount'),
        'type' => 'token',
        'options list' => 'commerce_discount_entity_list',
      ),
    ),
  );
  return $actions;
}


/**
 * Rules action: Apply free products discount with same quantity.
 *
 * @param EntityDrupalWrapper $wrapper
 *   Wrapped commerce_order entity type.
 * @param string $discount_name
 *   The name of the discount.
 */
function commerce_discount_free_products_same_quantity(EntityDrupalWrapper $wrapper, $discount_name) {
  $discount_wrapper = entity_metadata_wrapper('commerce_discount', $discount_name);
//dpm($discount_wrapper->value(),'discount wrapper');
//dpm($discount_wrapper->commerce_discount_offer->value(),'discount wrapper commerce_discount_offer');
//dpm($discount_wrapper->commerce_discount_offer->commerce_free_products->value(),'discount wrapper2');
  if (!$products = $discount_wrapper->commerce_discount_offer->commerce_free_products->value()) {
    return;
  }

  // Exit if the wrapper doesn't have a commerce_discounts property.
  if (!isset($wrapper->commerce_discounts)) {
    return;
  }
  $condition_pids=[];
  foreach ($discount_wrapper->inline_conditions->value() as $condition) {
//    dpm($condition,'condition');
    foreach ($condition['condition_settings']['products'] as $pid) {
      $condition_pids[]= $pid['product_id'];
      //$condition_products[$p_id] = $condition['condition_settings']['quantity'];
//      dpm($p_id,'pid');
    }
  }
//  dpm($condition_pids,'condition pid');

  // Set reference to the discount.
  $order = $wrapper->value();
//  dpm($order,'order');
  $delta = $wrapper->commerce_discounts->count();
  $order->commerce_discounts[LANGUAGE_NONE][$delta]['target_id'] = $discount_wrapper->discount_id->value();
  $line_item_pid_quantity=[];
  foreach ($order->commerce_line_items['und'] as $order_line_item_id){
    $line_item_id=$order_line_item_id['line_item_id'];
    $order_line_item=commerce_line_item_load($line_item_id);
    $line_item_pid_quantity[$order_line_item->commerce_product['und'][0]['product_id']]=$order_line_item->quantity;
//    dpm($order_line_item,'order line item');
  }
//  dpm($line_item_pid_quantity,'line item pid quantity');
  $quantity=1;
  foreach ($condition_pids as $condition_pid){
    if(isset($line_item_pid_quantity[$condition_pid])){
      $quantity=$line_item_pid_quantity[$condition_pid];
    }
  }
//  dpm($quantity,'quantity');
  // Loop on products and add each product to order line items.
//  dpm($products,'products');
  foreach ($products as $product) {
    $context = array(
      'commerce_discount_offer' => 'free_products',
      'discount_name' => $discount_name,
    );

    // Look for a product display that references this project. This is
    // imprecise but covers the vastly most common use case.
    $node_instances = field_info_instances('node');

    foreach ($node_instances as $bundle => $instances) {
      foreach ($instances as $instance) {
        $field = field_info_field($instance['field_name']);

        if ($field['type'] == 'commerce_product_reference') {
          $query = new EntityFieldQuery();
          $results = $query
            ->entityCondition('entity_type', 'node')
            ->fieldCondition($instance['field_name'], 'product_id', $product->product_id)
            ->propertyCondition('status', 1)
            ->execute();

          if (!empty($results['node'])) {
            $nids = array_keys($results['node']);

            // Just take the first one.
            $node = node_load(reset($nids));
            $uri = entity_uri('node', $node);

            // Build the context array in order to attach the display path on
            // product level.
            $context += array(
              'entity_id' => $node->nid,
              'entity_type' => 'node',
              'display_path' => $uri['path'],
            );

            // If we have found something, no need to continue.
            break;
          }
        }
      }
    }

    // This covers use cases where the above logic is insufficient.
    $discount = $discount_wrapper->value();
    drupal_alter('commerce_discount_free_product_context', $context, $product, $discount);

    $data = array('context' => $context);
    $line_item = commerce_product_line_item_new($product, $quantity, $order->order_id, $data, 'product_discount');

    $wrapper_line_item = entity_metadata_wrapper('commerce_line_item', $line_item);

    // Getting the product price and negate it for the discount component price.
    $product_unit_price = commerce_price_wrapper_value(entity_metadata_wrapper('commerce_product', $product), 'commerce_price');
    $discount_amount = array(
      'amount' => -$product_unit_price['amount'],
      'currency_code' => $product_unit_price['currency_code'],
    );

    commerce_discount_add_price_component($wrapper_line_item, $discount_name, $discount_amount);

    commerce_line_item_save($line_item);

    // Add the free product to order's line items.
    $wrapper->commerce_line_items[] = $line_item;
  }

  // Update the total order price, for the next rules condition (if any).
  commerce_order_calculate_total($order);
}


